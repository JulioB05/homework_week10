This application allows you to view my personal information and experience, it has an implementation in vercel:
https://homework-week10-dun.vercel.app/

You can find the following pages:

- Home
- About
- Projects
- Specific project
- Contact us
