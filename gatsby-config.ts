import type { GatsbyConfig } from "gatsby";


const config: GatsbyConfig = {
  siteMetadata: {
    title: `Homework-week10`,
    siteUrl: `https://www.yourdomain.tld`
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [{
    resolve: 'gatsby-plugin-google-analytics',
    options: {
      "trackingId": "123"
    }
  }, 
  "gatsby-plugin-image", 
  "gatsby-plugin-sitemap", 
  "gatsby-plugin-sharp", 
  "gatsby-transformer-sharp", 
  `gatsby-transformer-json`,
  `gatsby-plugin-typescript`,
   'gatsby-plugin-postcss',
   'gatsby-plugin-sass',
   `gatsby-plugin-postcss`,
 
  {
    resolve: 'gatsby-source-filesystem',
    options: {
      "name": "data",
      "path": "./data"
    }
  }
]

};

export default config;
