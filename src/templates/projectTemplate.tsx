import { graphql } from 'gatsby';
import React from 'react';
import Layout from '../components/layout';
import Project from '../components/project';
import { Data } from '../interfaces/project';

export const query = graphql`
  query ($slug: String!) {
    projectsJson(slug: { eq: $slug }) {
      title
      description
      url
      tags
      image {
        childImageSharp {
          gatsbyImageData(placeholder: BLURRED)
        }
      }
    }
  }
`;
interface ProjectTemplateProps {
  data: Data;
}
const ProjectTemplate = (props: ProjectTemplateProps) => {
  const { data } = props;
  const { title, description, image, url, tags } = data.projectsJson;
  const imageData = image.childImageSharp.gatsbyImageData;

  return (
    <Layout>
      <Project
        imageData={imageData}
        title={title}
        description={description}
        url={url}
        tags={tags}
      />
    </Layout>
  );
};

export default ProjectTemplate;
