export interface Projects {
  data: DataProjects;
  extensions: Extensions;
}

export interface DataProjects {
  allProjectsJson: AllProjectsJSON;
  allTagsJson: AllTagsJSON;
}

export interface AllProjectsJSON {
  edges: AllProjectsJSONEdge[];
}

export interface AllProjectsJSONEdge {
  node: PurpleNode;
}

export interface PurpleNode {
  title: string;
  slug: string;
  description: string;
  tags: Tag[];
  image: Image;
}

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface GatsbyImageData {
  layout: 'fixed' | 'fullWidth' | 'constrained';
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Placeholder {
  fallback: string;
}

export enum Tag {
  CSS = 'CSS',
  HTML = 'HTML',
  JS = 'JS',
}

export interface AllTagsJSON {
  edges: AllTagsJSONEdge[];
}

export interface AllTagsJSONEdge {
  node: FluffyNode;
}

export interface FluffyNode {
  name: string;
}

export interface Extensions {}
