export interface About {
  data: aboutData;
  extensions: Extensions;
}

export interface aboutData {
  allDataJson: AllDataJSON;
  allWorksJson: AllWorksJSON;
  allEducationJson: AllEducationJSON;
  allSkillsJson: AllSkillsJSON;
}

export interface AllDataJSON {
  edges: AllDataJSONEdge[];
}

export interface AllDataJSONEdge {
  node: PurpleNode;
}

export interface PurpleNode {
  name: string;
  label: string;
  email: string;
  phone: string;
  website: string;
  fullName: string;
  summary: string;
  image: Image;
}

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface GatsbyImageData {
  layout: 'fixed' | 'fullWidth' | 'constrained';
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Placeholder {
  fallback: string;
}

export interface AllEducationJSON {
  edges: AllEducationJSONEdge[];
}

export interface AllEducationJSONEdge {
  node: FluffyNode;
}

export interface FluffyNode {
  institution: string;
  area: string;
  studyType: string;
  startDateEdu: string;
  endDateEdu: string;
  image: Image;
}

export interface AllSkillsJSON {
  edges: AllSkillsJSONEdge[];
}

export interface AllSkillsJSONEdge {
  node: TentacledNode;
}

export interface TentacledNode {
  programming: string[] | null;
  languages: Language[] | null;
}

export interface Language {
  name: string;
  level: string;
}

export interface AllWorksJSON {
  edges: AllWorksJSONEdge[];
}

export interface AllWorksJSONEdge {
  node: StickyNode;
}

export interface StickyNode {
  company: string;
  position: string;
  startDate: string;
  endDate: string;
  summaryWork: string;
  highlights: string[];
  image: Image;
}

export interface Extensions {}
