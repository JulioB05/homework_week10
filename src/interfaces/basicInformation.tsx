export interface personalInformation {
  data: DataPersonalInformation;
  extensions: Extensions;
}

export interface DataPersonalInformation {
  allDataJson: AllPersonalInformationJSON;
}

export interface AllPersonalInformationJSON {
  edges: Edge[];
}

export interface Edge {
  node: Node;
}

export interface Node {
  name: string;
  fullName: string;
  label: string;
  email: string;
  phone: string;
  website: string;
  summary: string;
  image: Image;
}

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface GatsbyImageData {
  layout: 'fixed' | 'fullWidth' | 'constrained';
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Placeholder {
  fallback: string;
}

export interface Extensions {}
