export interface Project {
  data: Data;
  extensions: Extensions;
}

export interface Data {
  projectsJson: ProjectsJSON;
}

export interface ProjectsJSON {
  title: string;
  description: string;
  url: string;
  image: Image;
  tags: string[];
}

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface GatsbyImageData {
  layout: 'fixed' | 'fullWidth' | 'constrained';
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Placeholder {
  fallback: string;
}

export interface Extensions {}
