export interface Tags {
  data: DataTags;
  extensions: Extensions;
}

export interface DataTags {
  allTagsJson: AllTagsJSON;
}

export interface AllTagsJSON {
  edges: Edge[];
}

export interface Edge {
  node: Node;
}

export interface Node {
  name: string;
}

export interface Extensions {}
