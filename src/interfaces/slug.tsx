export interface SlugData {
  data: Data;
  extensions: Extensions;
}

export interface Data {
  allProjectsJson: AllProjectsJSON;
}

export interface AllProjectsJSON {
  edges: Edge[];
}

export interface Edge {
  node: Node;
}

export interface Node {
  slug: string;
}

export interface Extensions {}
