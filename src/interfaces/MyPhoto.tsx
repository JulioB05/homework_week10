export interface MyPhoto {
  data: DataMyPhoto;
  extensions: Extensions;
}

export interface DataMyPhoto {
  file: File;
}

export interface File {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface GatsbyImageData {
  layout: 'fixed' | 'fullWidth' | 'constrained';
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Placeholder {
  fallback: string;
}

export interface Extensions {}
