import {
  faFacebook,
  faInstagram,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'gatsby';
import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import './style.css';

const Header = () => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>
          <Link to="/" className="header-link">
            JB Portfolio
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Link to="/about/" className="header-link">
              About
            </Link>
            <Link to="/projects/" className="header-link">
              Projects
            </Link>
            <Link to="/contact/" className="header-link">
              Contact Us
            </Link>
          </Nav>
          <Nav>
            <Nav.Link href="https://www.facebook.com/">
              <FontAwesomeIcon icon={faFacebook} className="header-icon" />
            </Nav.Link>
            <Nav.Link
              href="https://www.linkedin.com/in/juliobasto"
              className="header-icon"
            >
              <FontAwesomeIcon icon={faLinkedin} className="header-icon" />
            </Nav.Link>
            <Nav.Link href="https://www.instagram.com/">
              <FontAwesomeIcon icon={faInstagram} className="header-icon" />
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
