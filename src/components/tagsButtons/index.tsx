import { graphql, useStaticQuery } from 'gatsby';
import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { DataTags } from '../../interfaces/tags';

export const data: DataTags = useStaticQuery(graphql`
  query Tags {
    allTagsJson {
      edges {
        node {
          name
        }
      }
    }
  }
`);
const tags = data.allTagsJson.edges;

interface TagsButtonsProps {}
const TagsButtons = (props: TagsButtonsProps) => {
  const {} = props;
  return (
    <ButtonGroup aria-label="Basic example">
      {tags.map((tag) => {
        const nameTag = tag.node.name;
        return <Button variant="secondary">{nameTag}</Button>;
      })}
    </ButtonGroup>
  );
};

export default TagsButtons;
