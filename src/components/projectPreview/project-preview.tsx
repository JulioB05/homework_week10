import { Link } from 'gatsby';
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image';
import React from 'react';
import Card from 'react-bootstrap/Card';
import './style.css';

interface ProjectPreviewProps {
  slug: string;
  imageData: IGatsbyImageData;
  title: string;
  description: string;
}

const ProjectPreview = (props: ProjectPreviewProps) => {
  const { slug, imageData, title, description } = props;

  return (
    <div className="col-12 col-md-6 col-lg-4 ">
      <Card className="card-space">
        <Link to={`/projects/${slug}/`} className={'link'}>
          <GatsbyImage alt={title} image={imageData} />
        </Link>
        <Card.Body>
          <Card.Title>
            <Link to={`/projects/${slug}/`} className={'link-title'}>
              {title}
            </Link>
          </Card.Title>
          <Card.Text>{description}</Card.Text>
        </Card.Body>
        <Card.Footer>
          <Card.Text>
            <Link to={`/projects/${slug}/`} className={'link-view'}>
              View this project &rarr;
            </Link>
          </Card.Text>
        </Card.Footer>
      </Card>
    </div>
  );
};

export default ProjectPreview;
