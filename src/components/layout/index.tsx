import React, { ReactNode } from 'react';
import Header from '../header';
import './style.css';

interface LayoutProps {
  children: ReactNode;
  grid?: boolean;
}

const Layout = (props: LayoutProps) => {
  const { children, grid } = props;
  return (
    <React.Fragment>
      <Header></Header>

      <main className="layout-content">{!grid && children}</main>

      {grid && (
        <div className="container">
          <div className="row space">{children}</div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Layout;
