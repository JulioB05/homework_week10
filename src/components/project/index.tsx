import {
  faCss3,
  faHtml5,
  faJs,
  faReact,
  faSass,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'gatsby';
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image';
import React from 'react';

interface ProjectProps {
  imageData: IGatsbyImageData;
  title: string;
  description: string;
  url: string;
  tags: string[];
}

const Project = (props: ProjectProps) => {
  const { imageData, title, description, url, tags } = props;

  return (
    <section>
      <div className="card mb-3 about-container">
        <div className="row g-0 ">
          <div className="col-md-4">
            <GatsbyImage
              image={imageData}
              alt={title}
              className="project-image"
            ></GatsbyImage>
          </div>
          <div className="col-md-8">
            <div className="card-body about-information">
              <h5 className="card-title">{title}</h5>
              <p className="card-text">{description}</p>

              <div className="project-tags-container">
                {tags.map((tag) => {
                  switch (tag) {
                    case 'HTML': {
                      return <FontAwesomeIcon icon={faHtml5} className="tag" />;
                    }
                    case 'CSS': {
                      return <FontAwesomeIcon icon={faCss3} className="tag" />;
                    }
                    case 'React': {
                      return <FontAwesomeIcon icon={faReact} className="tag" />;
                    }
                    case 'JavaScript': {
                      return <FontAwesomeIcon icon={faJs} className="tag" />;
                    }
                    case 'SASS': {
                      return <FontAwesomeIcon icon={faSass} className="tag" />;
                    }
                    case 'all': {
                      return '';
                    }
                  }
                  return (
                    <p key={tag} className="card-text">
                      {tag}
                    </p>
                  );
                })}
              </div>

              <p className="card-text ">
                <Link to={url}>View this project online &rarr;</Link>
              </p>
              <p className="card-text ">
                <Link to="/projects">&larr; Back to all projects</Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Project;
