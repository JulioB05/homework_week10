import { graphql, HeadFC, useStaticQuery } from 'gatsby';
import * as React from 'react';
import { useMemo, useState } from 'react';
import { Button } from 'react-bootstrap';
import Layout from '../components/layout';
import ProjectPreview from '../components/projectPreview/project-preview';
import { DataProjects, Tag } from '../interfaces/projects';
import './styles.css';

export default () => {
  const [filterTag, setFilterTag] = useState('all');
  const data: DataProjects = useStaticQuery(graphql`
    {
      allProjectsJson {
        edges {
          node {
            title
            slug
            description
            tags
            image {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
        }
      }
      allTagsJson {
        edges {
          node {
            name
          }
        }
      }
    }
  `);

  const projects = data.allProjectsJson.edges;
  const tags = data.allTagsJson.edges;

  const filterFunction = (tag: string) => {
    setFilterTag(tag);
  };

  const functionClean = () => {
    setFilterTag('all');
  };
  const filteredProjects = useMemo(() => {
    const filter = filterTag as Tag;
    return projects.filter((project) => project.node.tags.includes(filter));
  }, [filterTag]);

  return (
    <Layout grid>
      <div className="tags-buttons">
        {tags.map((tag) => {
          const nameTag = tag.node.name;
          return (
            <Button
              variant="secondary"
              key={nameTag}
              onClick={() => filterFunction(nameTag)}
              className="btn btn-secondary"
            >
              {nameTag}
            </Button>
          );
        })}
        <Button
          variant="primary"
          className="btn btn-primary"
          onClick={functionClean}
        >
          Clean Filter
        </Button>
      </div>

      {filteredProjects.map((project) => {
        const { title, description, slug, image } = project.node;

        const imageData = image.childImageSharp.gatsbyImageData;

        return (
          <ProjectPreview
            key={title}
            slug={slug}
            imageData={imageData}
            title={title}
            description={description}
          />
        );
      })}
    </Layout>
  );
};

export const Head: HeadFC = () => <title>Projects</title>;
