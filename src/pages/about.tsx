import {
  faCss3,
  faFacebook,
  faGitlab,
  faHtml5,
  faInstagram,
  faJs,
  faLinkedin,
  faReact,
  faSass,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { graphql, HeadFC, useStaticQuery } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import React from 'react';
import {
  Accordion,
  Card,
  Col,
  ListGroup,
  Nav,
  Row,
  Tab,
} from 'react-bootstrap';
import Layout from '../components/layout';
import { aboutData } from '../interfaces/aboutData';
import './styles.css';

export default () => {
  const data: aboutData = useStaticQuery(graphql`
    {
      allDataJson {
        edges {
          node {
            name
            label
            email
            phone
            website
            fullName
            summary
            image {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
        }
      }
      allWorksJson {
        edges {
          node {
            company
            position
            startDate
            endDate
            summaryWork
            highlights
            image {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
        }
      }
      allEducationJson {
        edges {
          node {
            institution
            area
            studyType
            startDateEdu
            endDateEdu
            image {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
        }
      }
      allSkillsJson {
        edges {
          node {
            programming
            languages {
              name
              level
            }
          }
        }
      }
    }
  `);

  const dataBasic = data.allDataJson.edges[0].node;
  const dataWorks = data.allWorksJson.edges;
  const dataEducation = data.allEducationJson.edges;
  const dataSkills = data.allSkillsJson.edges[0].node;

  const { label, email, summary, fullName, website } = dataBasic;
  const { programming, languages } = dataSkills;
  const image = dataBasic.image.childImageSharp.gatsbyImageData;
  return (
    <Layout>
      <section>
        <div className="card mb-3 about-container">
          <div className="row g-0 ">
            <div className="col-md-4">
              <GatsbyImage
                image={image}
                alt={'Myphoto'}
                className="data-img"
              ></GatsbyImage>
            </div>
            <div className="col-md-8">
              <div className="card-body about-information">
                <h5 className="card-title">{fullName}</h5>
                <p className="card-text">{label}</p>
                <p className="card-text">{summary}</p>

                <div className="about-stack-container">
                  <FontAwesomeIcon icon={faCss3} className="stack" />
                  <FontAwesomeIcon icon={faReact} className="stack" />
                  <FontAwesomeIcon icon={faJs} className="stack" />
                  <FontAwesomeIcon icon={faHtml5} className="stack" />
                  <FontAwesomeIcon icon={faGitlab} className="stack" />
                  <FontAwesomeIcon icon={faSass} className="stack" />
                </div>

                <p className="card-text ">
                  <div className="about-stack-container">
                    <small className="text-muted">{email}</small>
                    <small className="text-muted">{website}</small>
                    <Nav.Link href="https://www.facebook.com/">
                      <FontAwesomeIcon
                        icon={faFacebook}
                        className="social-stack"
                      />
                    </Nav.Link>
                    <Nav.Link href="https://www.linkedin.com/in/juliobasto">
                      <FontAwesomeIcon
                        icon={faLinkedin}
                        className="social-stack"
                      />
                    </Nav.Link>
                    <Nav.Link href="https://www.instagram.com/">
                      <FontAwesomeIcon
                        icon={faInstagram}
                        className="social-stack"
                      />
                    </Nav.Link>
                  </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Accordion defaultActiveKey={['0']} flush>
        <Accordion.Item eventKey="0">
          <Accordion.Header className="section-title">
            <h2 className="section-title">Experience</h2>
          </Accordion.Header>
          <Accordion.Body>
            <section className="about-section-works">
              <Accordion defaultActiveKey="0" flush>
                {dataWorks.map((work) => {
                  const {
                    company,
                    position,
                    startDate,
                    endDate,
                    summaryWork,
                    highlights,
                  } = work.node;
                  const imageWork =
                    work.node.image.childImageSharp.gatsbyImageData;
                  return (
                    <Accordion.Item eventKey={company} key={company}>
                      <Accordion.Header>{company}</Accordion.Header>
                      <Accordion.Body>
                        <div className="card mb-3 about-container">
                          <div className="row g-0 ">
                            <div className="col-md-4">
                              <GatsbyImage
                                image={imageWork}
                                alt={'Myphoto'}
                                className="work-img"
                              ></GatsbyImage>
                            </div>
                            <div className="col-md-8">
                              <div className="card-body about-information">
                                <h5 className="card-title">{company}</h5>
                                <p className="card-text">{position}</p>
                                <p className="card-text">{summaryWork}</p>
                                <p className="card-text"> {highlights}</p>

                                <p className="card-text ">
                                  <div className="about-stack-container">
                                    <small className="text-muted">
                                      {startDate}
                                    </small>
                                    <small className="text-muted">
                                      {endDate}
                                    </small>
                                  </div>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Accordion.Body>
                    </Accordion.Item>
                  );
                })}
              </Accordion>
            </section>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>

      <Accordion defaultActiveKey={['1']} flush>
        <Accordion.Item eventKey="1">
          <Accordion.Header>
            <h2 className="section-title">Education</h2>
          </Accordion.Header>
          <Accordion.Body>
            <section className="about-section-works">
              <Row xs={1} md={2} className="g-4">
                {dataEducation.map((edu) => {
                  const {
                    institution,
                    area,
                    studyType,
                    startDateEdu,
                    endDateEdu,
                  } = edu.node;
                  const imageEducation =
                    edu.node.image.childImageSharp.gatsbyImageData;
                  return (
                    <Col className="col-lg-6" key={institution}>
                      <Card>
                        <GatsbyImage
                          image={imageEducation}
                          alt={'Myphoto'}
                          className="education-image"
                        ></GatsbyImage>
                        <Card.Body>
                          <Card.Title>{institution}</Card.Title>
                          <Card.Text>{area}</Card.Text>
                          <Card.Text>{studyType}</Card.Text>
                          <div className="about-date-container">
                            <small className="text-muted">{startDateEdu}</small>
                            <small className="text-muted">{endDateEdu}</small>
                          </div>
                        </Card.Body>
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </section>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>

      <Accordion defaultActiveKey={['2']} flush>
        <Accordion.Item eventKey="2">
          <Accordion.Header>
            <h2 className="section-title">Skills</h2>
          </Accordion.Header>
          <Accordion.Body>
            <section className="about-section-works">
              <Tab.Container
                id="list-group-tabs-example"
                defaultActiveKey="#link1"
              >
                <Row>
                  <Col sm={4}>
                    <ListGroup>
                      <ListGroup.Item action href="#link1">
                        Programming
                      </ListGroup.Item>
                      <ListGroup.Item action href="#link2">
                        Languages
                      </ListGroup.Item>
                    </ListGroup>
                  </Col>
                  <Col sm={8}>
                    <Tab.Content>
                      <Tab.Pane eventKey="#link1">
                        <div className="about-skills">
                          {programming?.map((word) => {
                            return <p key={word}>{word}</p>;
                          })}
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey="#link2">
                        <div className="about-skills">
                          {languages?.map((type) => {
                            const { name, level } = type;
                            return (
                              <p key={name}>
                                Name: {name} - level{level}
                              </p>
                            );
                          })}
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </Col>
                </Row>
              </Tab.Container>
            </section>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>

      <div className="space-end"></div>
    </Layout>
  );
};

export const Head: HeadFC = () => <title>About</title>;
