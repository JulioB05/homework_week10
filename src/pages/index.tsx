import { graphql, HeadFC, Link, useStaticQuery } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import React from 'react';
import Layout from '../components/layout';
import { DataPersonalInformation } from '../interfaces/basicInformation';
import './styles.css';

export default () => {
  const data: DataPersonalInformation = useStaticQuery(graphql`
    {
      allDataJson {
        edges {
          node {
            name
            label
            email
            phone
            website
            fullName
            summary
            image {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
        }
      }
    }
  `);

  const dataBasic = data.allDataJson.edges[0].node;

  const { name, label, image } = dataBasic;
  const imageLazy = image.childImageSharp.gatsbyImageData;
  return (
    <Layout>
      <div className="personal-data">
        <div className="section-center data-center">
          <article className="data-info">
            <div className="underline">
              <h1 className="data-title">I'm {name}</h1>
              <h4 className="data-label">{label}</h4>

              <Link to="/contact" className="data-contact-link">
                Contact me
              </Link>
            </div>
          </article>
          <GatsbyImage
            image={imageLazy}
            alt={'Myphoto'}
            className="data-img"
          ></GatsbyImage>
        </div>
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <title>Home</title>;
