import { GatsbyNode } from 'gatsby';
import { resolve } from 'path';
import { Data } from './src/interfaces/slug';

export const createPages: GatsbyNode['createPages'] = async ({ graphql, actions, reporter }) => {
  const {createPage} = actions;
  const result = await graphql(`  
  query SlugData{
    allProjectsJson {
      edges {
        node {
          slug
        }
      }
    }
  } 
  `);

  if(result.errors){
    reporter.panic('There was a problem loading your projects!')
    return;
  }

  const projects = (result.data as Data).allProjectsJson.edges;

  projects.forEach(({ node:project }) => {
    const slug = project.slug;

    actions.createPage({
      path: `/projects/${slug}/`,
      component: resolve('./src/templates/projectTemplate.tsx'),
      context: { slug },
     
    }); 
  });
}